<h1 style="text-align:center">Đông trùng hạ thảo và những lưu ý quan trọng khi dùng</h1>

<p style="text-align: justify;">Đông trùng hạ thảo là loại dược phẩm tiến cung từ hàng ngàn năm trước. Chỉ có vua chúa, tầng lớp quý tộc, quan lại mới có cơ hội để sử dụng đông trùng nhằm bổi bổ cơ thể, phục hồi sức khỏe, nhất là những quý ông. Cho đến nay, giá trị dược chất của đông trùng hạ thảo đã được cả Đông y và Tây y thừa nhận, việc sử dụng đúng cách sẽ giúp người bệnh có sức khỏe tuyệt vời. Tuy nhiên, nếu thiếu hiểu biết hoặc chưa tìm hiểu kỹ đã sử dụng thì có thể gây ra những hậu quả khôn lường. Dưới đây là những điều tối kỵ khi sử dụng đông trùng hạ thảo bạn cần đặc biệt lưu tâm.</p>
<p style="text-align: center;"><img class="aligncenter" src="https://dongtrungtaytang.com.vn/wp-content/uploads/2020/06/dong-trung-tay-tang-na-khuc-tu-nhien.jpg" /></p>
<p style="text-align: justify;">Để xem các LOẠI đông trùng hạ thảo vui lòng Click vào đường dẫn sau: <a class="seoquake-nofollow" href="https://dongtrungtaytang.com.vn/dong-trung-ha-thao" target="_blank" rel="nofollow noreferrer noopener">https://dongtrungtaytang.com.vn/dong-trung-ha-thao</a></p>

<h2>Những giá trị dược chất của đông trùng hạ thảo</h2>
<p style="text-align: justify;">Lợi ích sức khỏe của đông trùng hạ thảo được nhắc đến trong nhiều đề tài, nghiên cứu của các nhà khoa học Mỹ, Nhật Bản, New Zeland. Theo đó, đông trùng có giá trị lớn dối với sức khỏe, chủ yếu là bồi bổ cơ thể, ngăn ngừa, hỗ trợ giảm triệu chứng và biến chứng của một số bệnh. Cụ thể:</p>

<ul>
 	<li>Bồi bổ cơ thể, tăng cường sức khỏe cho người già. Tăng cường trí lực cho người cao tuổi.</li>
 	<li>Nâng cao hệ miễn dịch, sức đề kháng cho cơ thể. Từ đó phòng ngừa bệnh tật hiệu quả.</li>
 	<li style="text-align: justify;">Cải thiện sức khỏe, phục hồi cơ thể cho những bệnh nhân đang trong quá trình hóa - xạ trị. Hạn chế những tác động của hóa chất đến cơ thể, giảm đau đớn cho bệnh nhân.</li>
 	<li style="text-align: justify;">Bổ phổi, có lợi cho những người ho lâu ngày, hen suyễn, người mắc bệnh về đường hô hấp.</li>
 	<li style="text-align: justify;">Ngừa bệnh tim mạch, gan, thận, bệnh về hệ tuần hoàn.</li>
 	<li style="text-align: justify;">Cải thiện sức khỏe cho cả nam và nữ. Đặc biệt hiệu quả với sức khỏe nam giới.</li>
 	<li style="text-align: justify;">Điều hòa kinh nguyệt, rối loạn nội tiết tố ở nữ giới. Mang lại nhiều lợi ích cho phụ nữ tiền mãn kinh.</li>
</ul>
<h2>Những điều kiêng kỵ khi sử dụng đông trùng hạ thảo</h2>
<p style="text-align: justify;">Tuy là dược liệu tốt cho sức khỏe nhưng người dùng cũng cần nắm được những kiêng kỵ để dược chất của đông trùng hạ thảo phát huy giá trị. Dưới đây là những lưu ý chi tiết.</p>
<p style="text-align: center;"><img class="aligncenter" src="https://dongtrungtaytang.com.vn/wp-content/uploads/2020/03/luu-y-khi-dung-dong-trung-ha-thao.jpg" /></p>
<p style="text-align: center;"><em>Một số lưu ý quan trọng khi dùng đông trùng hạ thảo</em></p>

<h3>Về đối tượng sử dụng</h3>
<p style="text-align: justify;">Mặc dù đông trùng hạ thảo tốt cho sức khỏe nhưng không phải ai cũng có thể sử dụng. KHÔNG dùng đông trùng hạ thảo cho các đối tượng sau:</p>

<ul>
 	<li>Trẻ em dưới 13 tuổi</li>
 	<li><a href="https://www.vinmec.com/vi/tin-tuc/thong-tin-suc-khoe/roi-loan-dong-mau-la-gi-cac-chi-so-roi-loan-dong-mau/">Người bị rối loạn đông máu</a></li>
 	<li>Người chuẩn bị phẫu thuật</li>
 	<li>Thai phụ đang cho con bú, nữ giới trong đang thời kỳ kinh nguyệt.</li>
</ul>
<h3>Về liều lượng sử dụng</h3>
<p style="text-align: justify;">Để đảm bảo sức khỏe, người dùng nên chú ý dùng đông trùng hạ thảo theo hướng dẫn của thầy thuốc, người có kiến thức chuyên môn. Cụ thể:</p>

<ul>
 	<li>Liều dùng khuyến nghị: 1-2g/ngày</li>
 	<li style="text-align: justify;">Mặc dù là dược liệu bồi bổ sức khỏe nhưng nên sử dụng ĐTHT theo liều lượng hợp lý. Tùy thuộc vào mục đích sử dụng mà mỗi người sẽ nên dùng với liều lượng khác nhau. Ví dụ, người với mục đích bồi bổ chỉ nên dùng 1-2g, nhưng người muốn giảm triệu chứng, biến chứng của bệnh thì cần dùng liều cao hơn. Tuy nhiên, nên dùng bao nhiêu gam đông trùng hạ thảo mỗi ngày cần có hướng dẫn cụ thể của bác sĩ.</li>
</ul>
<h3><strong>Trong chế biến:</strong></h3>
<ul>
 	<li style="text-align: justify;"><strong>Không hầm, sắc nấu đông trùng bằng nồi kim loại</strong>: Việc sử dụng nồi kim loại để chế biến đông trùng hạ thảo có thể tạo điều kiện cho dược chất của ĐTHT kết hợp với kim loại gây ảnh hưởng xấu cho sức khỏe. Phản ứng hóa học giữa 2 chất cũng làm giảm đáng kể giá trị dược chất của đông trùng. Thay vì ấm kim loại, nồi kim loại, nên dùng nồi sứ để đun ĐTHT, vừa bảo vệ sức khỏe lại bảo toàn nguyên vẹn dược chất của đông trùng.</li>
 	<li style="text-align: justify;"><strong>Không đun ĐTHT trong thời gian quá lâu</strong>: Các chuyên gia khuyến khích sử dụng đông trùng trực tiếp để bảo toàn nguyên dược chất. Cũng có thể hầm, nấu cháo, canh ĐTHT nhưng phải đun nấu đúng cách, không nên hầm quá lâu với nhiệt độ cao. Bởi điều đó có thể gây tổn hao lượng lớn dược chất của ĐTHT. Nên chú ý điều này để có món ăn bổ dưỡng cho sức khỏe.</li>
</ul>
<blockquote>
<p style="text-align: justify;">Xem một số hướng dẫn chế biến đông trùng chuẩn trong bài viết: <a href="https://dongtrungtaytang.mystrikingly.com/blog/mon-an-dong-trung-ha-thao">Tổng hợp các món ăn từ đông trùng hạ thảo "đại bổ" cho sức khỏe</a></p>
</blockquote>
<p style="text-align: center;"><img src="https://dongtrungtaytang.com.vn/wp-content/uploads/2020/03/cach-che-bien-dong-trung-ha-thao-nguyen-con.jpg" /></p>
<p style="text-align: center;"><em>Một số cách chế biến đông trùng hạ thảo chuẩn - hiệu quả</em></p>

<h3><strong>Trong sử dụng:</strong></h3>
<p style="text-align: justify;">Theo chia sẻ của lương y <a href="https://dongtrungtaytang.com.vn/author/thay-thuoc-tran-quoc-binh/">Trần Quốc Bình</a> sử dụng đông trùng hạ thảo để bồi bổ cơ thể, hỗ trợ phục hồi chức năng, giảm triệu chứng của bệnh nên chú ý:</p>

<ul>
 	<li>Trong quá trình sử dụng cần tuyệt đối tránh đồ cay nóng. Không sử dụng song song đông trùng và đồ ăn cay nóng, nhiều dầu mỡ.</li>
 	<li style="text-align: justify;">Những người ốm yếu, không thể ăn trực tiếp ĐTHT có thể đem hầm nhừ với cháo để hệ tiêu hóa dễ hấp thu.</li>
 	<li style="text-align: justify;">Tuyệt đối không tự ý sử dụng, tăng liều lượng hoặc kết hợp với những dược liệu khác khi chưa có chỉ định của thầy thuốc.</li>
</ul>
<p style="text-align: justify;">Như vậy, để đảm bảo sức khỏe, khi sử dụng đông trùng hạ thảo bạn cần nắm được những điều kiêng kỵ phía trên. Đặc biệt là trước khi sử dụng nên tham khảo ý kiến của thầy thuốc, người có kiến thức chuyên môn, tuyệt đối không tự ý sử dụng hoặc tăng liều lượng khi chưa được hướng dẫn để tránh những phản ứng ngược và tác hại không mong muốn.</p>
